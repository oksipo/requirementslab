﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Models.Entities
{
    public class User : IdentityUser
    {
        public string FullName { get; set; }

        public List<Result> Results { get; set; }
    }
}