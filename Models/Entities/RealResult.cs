﻿using Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Entities
{
    public class RealResult
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(Requirement))]
        public long RequirementId { get; set; }
        public virtual Requirement Requirement { get; set; }

        [ForeignKey(nameof(Result))]
        public long ResultId { get; set; }
        public virtual Result Result { get; set; }


        public Category Category { get; set; }
    }
}