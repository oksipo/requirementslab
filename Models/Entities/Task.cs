﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models.Entities
{
    public class Task
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual List<Requirement> Requirements { get; set; }

        public virtual List<Result> Results { get; set; }
    }
}