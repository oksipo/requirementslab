﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Entities
{
    public class Requirement
    {
        [Key]
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        [ForeignKey(nameof(ExpectedResult))]
        public long ExpectedResultId { get; set; }
        public virtual ExpectedResult ExpectedResult { get; set; }

        [ForeignKey(nameof(Task))]
        public long TaskId { get; set; }
        public virtual Task Task { get; set; }

        public List<RealResult> RealResults { get; set; }
    }
}