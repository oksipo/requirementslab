﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Entities
{
    public class Result
    {
        [Key]
        public long Id { get; set; }

        [ForeignKey(nameof(User))]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey(nameof(Task))]
        public long TaskId { get; set; }
        public virtual Task Task { get; set; }

        public virtual List<RealResult> RealResults { get; set; }

        public string Recommendations { get; set; }

        public DateTime DateCreated { get; set; }
    }
}