﻿using Models.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Entities
{
    public class ExpectedResult
    {
        [Key]
        public long Id { get; set; }
        public virtual Requirement Requirement { get; set; }

        public Category Category { get; set; }
    }
}