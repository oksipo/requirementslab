import React, { Component } from "react";
import { BrowserRouter as Router, Route, withRouter } from "react-router-dom";
import { Registration, Login } from "./AuthorizationModule";
import {
  Main,
  Analysis,
  Ierarchical,
  Task,
  Comparison,
  Statystics,
  AddTask
} from "./MainModule";
import "./App.css";
import "antd/dist/antd.css";
import { LoginInterceptor } from "./LoginInterceptor";
import { Tasks } from "./MainModule/components/Tasks";
import { AdminTasks } from "./MainModule/components/AdminTasks";
import { EditTask } from "./MainModule/components/EditTask";

const axios = require("axios");

class App extends Component {
  componentDidMount() {
    if (localStorage.getItem("token")) {
      axios.defaults.headers.common["Authorization"] = localStorage.getItem(
        "token"
      );
    }
  }

  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Route path="/register/" component={Registration} />
            <Route path="/login/" component={Login} />
            <Route path="/main/" component={Main} />
            <Route path="/analysis/" component={Analysis} />
            <Route path="/ierarchical/" component={Ierarchical} />
            <Route path="/task/:id" component={Task} />
            <Route path="/comparison/:id" component={Comparison} />
            <Route path="/statystics/" component={Statystics} />
            <Route path="/addTask/" component={AddTask} />
            <Route path="/editTask/:id" component={EditTask} />
            <Route path="/tasks/" component={Tasks} />
            <Route path="/adminTasks/" component={AdminTasks} />
            <Route path="" component={LoginInterceptor} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
