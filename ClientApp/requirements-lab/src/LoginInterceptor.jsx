import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";

import { BASE_URL } from "./BaseConstants";

const axios = require("axios");

class LoginInterceptorGeneral extends PureComponent {
  componentDidMount() {
    if (localStorage.getItem("token")) {
      axios.defaults.headers.common["Authorization"] = localStorage.getItem(
        "token"
      );
      axios
        .get(`${BASE_URL}auth/isLoggedIn`)
        .then(responce => {
          if (!responce.data) {
            localStorage.removeItem("token");
            this.props.history.push("/login");
          } else {
            // this.props.history.push("/main");
          }
        })
        .catch(() => {
          localStorage.removeItem("token");
          this.props.history.push("/login");
        });
    } else {
      this.props.history.push("/login");
    }
  }

  render() {
    return "";
  }
}

export const LoginInterceptor = withRouter(LoginInterceptorGeneral);
