import React, { PureComponent } from "react";
import { Form, Input, Button, Checkbox, message } from "antd";
import { PageWrapper } from "../../common";
import { BASE_URL } from "../../BaseConstants";
import { Link, withRouter } from "react-router-dom";
const axios = require("axios");

const FormItem = Form.Item;
class RegistrationGeneral extends PureComponent {
  state = {
    isLoading: false
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({ isLoading: true }, () => {
          axios
            .post(`${BASE_URL}auth/register`, values)
            .then(response => {
              this.setState({ isLoading: false });
              localStorage.setItem("token", `Bearer ${response.data}`);
              axios.defaults.headers.common[
                "Authorization"
              ] = localStorage.getItem("token");
              message.success("Реєстрація успішна!");
              this.props.history.push("/main");
            })
            .catch(() => {
              message.error(
                "Помилка реєстрації. Перевірте правильність введених даних"
              );
              this.setState({ isLoading: false });
            });
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 3 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    };

    const noLabelLayout = {
      labelCol: {
        xs: { span: 0 },
        sm: { span: 0 }
      },
      wrapperCol: {
        xs: { span: 11 },
        sm: { span: 11 }
      }
    };

    return (
      <PageWrapper
        span={10}
        offset={8}
        header="Реєстрація"
        isLoading={this.state.isLoading}
      >
        <Form onSubmit={this.onSubmit}>
          <FormItem {...formItemLayout} label="E-mail" colon={false}>
            {getFieldDecorator("email", {
              rules: [
                {
                  type: "email",
                  message: "Будь-ласка, введіть правильну пошту"
                },
                { required: true, message: "Будь ласка, введіть пошту" }
              ]
            })(<Input placeholder="E-mail" type="email" />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Пароль" colon={false}>
            {getFieldDecorator("password", {
              rules: [{ required: true, message: "Будь ласка, введіть пароль" }]
            })(<Input placeholder="Пароль" type="password" />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Ім'я" colon={false}>
            {getFieldDecorator("fullName", {
              rules: [
                { required: true, message: "Будь ласка, введіть ваше ім'я" }
              ]
            })(<Input placeholder="Ім'я" />)}
          </FormItem>
          <FormItem {...noLabelLayout}>
            {getFieldDecorator("isAdmin", {
              valuePropName: "checked"
            })(<Checkbox>Я адміністратор</Checkbox>)}
          </FormItem>
          <Button type="primary" htmlType="submit">
            Зареєструватися
          </Button>
          <br />
          <Link to="/login">Увійти</Link>
        </Form>
      </PageWrapper>
    );
  }
}

export const Registration = withRouter(Form.create()(RegistrationGeneral));
