import React, { PureComponent } from "react";
import { Form, Input, Button, message } from "antd";
import { PageWrapper } from "../../common";
import { BASE_URL } from "../../BaseConstants";
import { Link, withRouter } from "react-router-dom";

const axios = require("axios");

const FormItem = Form.Item;
class LoginGeneral extends PureComponent {
  state = {
    isLoading: false
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.setState({ isLoading: true }, () => {
          axios
            .post(`${BASE_URL}auth/login`, values)
            .then(response => {
              this.setState({ isLoading: false });
              localStorage.setItem("token", `Bearer ${response.data}`);
              axios.defaults.headers.common[
                "Authorization"
              ] = localStorage.getItem("token");
              message.success("Вхід успішний!");
              this.props.history.push("/main");
            })
            .catch(() => {
              message.error(
                "Помилка входу. Перевірте правильність введених даних"
              );
              this.setState({ isLoading: false });
            });
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 3 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 20 }
      }
    };

    const noLabelLayout = {
      labelCol: {
        xs: { span: 0 },
        sm: { span: 0 }
      },
      wrapperCol: {
        xs: { span: 11 },
        sm: { span: 11 }
      }
    };

    return (
      <PageWrapper
        span={10}
        offset={8}
        header="Вхід"
        isLoading={this.state.isLoading}
      >
        <Form onSubmit={this.onSubmit}>
          <FormItem {...formItemLayout} label="E-mail" colon={false}>
            {getFieldDecorator("email", {
              rules: [
                {
                  type: "email",
                  message: "Будь-ласка, введіть правильну пошту"
                },
                { required: true, message: "Будь ласка, введіть пошту" }
              ]
            })(<Input placeholder="E-mail" type="email" />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Пароль" colon={false}>
            {getFieldDecorator("password", {
              rules: [{ required: true, message: "Будь ласка, введіть пароль" }]
            })(<Input placeholder="Пароль" type="password" />)}
          </FormItem>
          <Button type="primary" htmlType="submit">
            Увійти
          </Button>
          <br />
          <Link to="/register">Зареєструватися</Link>
        </Form>
      </PageWrapper>
    );
  }
}

export const Login = withRouter(Form.create()(LoginGeneral));
