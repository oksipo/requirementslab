import React, { PureComponent } from "react";
import { Col, Row ,Divider, Spin} from "antd";

import "./pagewrapper.css";

export class PageWrapper extends PureComponent {
  render() {
    return (
      <Row>
        <Col span={this.props.span} offset={this.props.offset}>
          <div className="base-wr">
          <div className = "spinner" style= {{visibility:this.props.isLoading?"visible":"hidden"}}>
          <Spin spinning />
          </div>
          <h3>{this.props.header}</h3>
          <Divider style={{marginTop:"5px",marginBottom:"5px"}}/>
          {this.props.children}
          </div>
        </Col>
      </Row>
    );
  }
}
