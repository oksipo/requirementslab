import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { PageWrapper } from "../../common";
import { Row, Button, Col, message, Collapse, Divider } from "antd";
import { DragDropContainer, DropTarget } from "react-drag-drop-container";
import { Requirement } from "./Requirement";
import { BASE_URL } from "../../BaseConstants";
import "../styles/main.css";

const axios = require("axios");

class TasksGeneral extends PureComponent {
  state = {
    tasks: [],
    loader: false
  };

  componentDidMount() {
    this.setState({ loader: true }, () => {
      axios.get(`${BASE_URL}task/`).then(({ data }) => {
        this.setState({
          tasks: data,
          loader: false
        });
      });
    });
  }

  render() {
    return (
      <PageWrapper
        span={16}
        offset={4}
        header="Оберіть завдання"
        isLoading={this.state.loader}
      >
        <div style={{ height: "600px", overflowY: "auto" }}>
          <Collapse>
            {this.state.tasks.map(x => (
              <Collapse.Panel header={x.name}>
                <p style={{ textAlign: "left" }}>
                  <b>Опис: </b>
                  {x.description}
                </p>
                <Divider style={{ margin: "2px" }} />
                <Button
                  type="primary"
                  style={{ float: "right", marginBottom: "5px" }}
                  onClick={() => this.props.history.push(`/task/${x.id}`)}
                >
                  Виконати
                </Button>
              </Collapse.Panel>
            ))}
          </Collapse>
        </div>
      </PageWrapper>
    );
  }
}

export const Tasks = withRouter(TasksGeneral);
