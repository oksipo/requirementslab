import React, { PureComponent } from "react";
import { DragDropContainer } from "react-drag-drop-container";

export class Requirement extends PureComponent {
  render() {
    return (
      <DragDropContainer dragData={this.props.requirement} targetKey="req">
        <div
          style={{
            border: "solid 1px grey",
            marginBottom: "5px",
            marginRight: "5px",
            width: "140px",
            background: "white"
          }}
        >
          <b>{this.props.requirement.name}</b>
          <br />
          {this.props.requirement.description}
        </div>
      </DragDropContainer>
    );
  }
}
