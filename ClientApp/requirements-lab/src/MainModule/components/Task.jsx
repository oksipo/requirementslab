import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { PageWrapper } from "../../common";
import { Row, Button, Col, message } from "antd";
import { DragDropContainer, DropTarget } from "react-drag-drop-container";
import { Requirement } from "./Requirement";
import { BASE_URL } from "../../BaseConstants";
import "../styles/main.css";

const axios = require("axios");

class TaskGeneral extends PureComponent {
  state = {
    unClassified: [],
    business: [],
    customer: [],
    functional: [],
    nonFunctional: [],
    loader: false,
    name: ""
  };

  componentDidMount() {
    this.setState({ loader: true }, () => {
      axios
        .get(`${BASE_URL}task/${this.props.match.params.id}`)
        .then(({ data }) => {
          this.setState({
            name: data.name,
            unClassified: data.requirements,
            loader: false
          });
        });
    });
  }

  moveTo = (requirement, category) => {
    let {
      unClassified,
      business,
      customer,
      functional,
      nonFunctional
    } = this.state;
    unClassified = unClassified.filter(x => x.id !== requirement.id);
    business = business.filter(x => x.id !== requirement.id);
    customer = customer.filter(x => x.id !== requirement.id);
    functional = functional.filter(x => x.id !== requirement.id);
    nonFunctional = nonFunctional.filter(x => x.id !== requirement.id);
    if (category === "unClassified") {
      unClassified.push(requirement);
    }
    if (category === "business") {
      business.push(requirement);
    }
    if (category === "customer") {
      customer.push(requirement);
    }
    if (category === "functional") {
      functional.push(requirement);
    }
    if (category === "nonFunctional") {
      nonFunctional.push(requirement);
    }
    this.setState({
      unClassified,
      business,
      customer,
      functional,
      nonFunctional
    });
  };

  validateAndSave = () => {
    this.setState({ loader: true }, () => {
      if (this.state.unClassified.length > 0) {
        message.error("Не всі вимоги класифіковано");
        this.setState({ loader: false });
      } else {
        axios
          .post(`${BASE_URL}task/result`, {
            taskId: this.props.match.params.id,
            businessRequirements: this.state.business.map(x => x.id),
            customerRequirements: this.state.customer.map(x => x.id),
            functionalRequirements: this.state.functional.map(x => x.id),
            nonFunctionalRequirements: this.state.nonFunctional.map(x => x.id)
          })
          .then(({ data }) => {
            this.props.history.push(`/comparison/${data}`);
          });
      }
    });
  };

  render() {
    return (
      <PageWrapper
        span={16}
        offset={4}
        header={this.state.name}
        isLoading={this.state.loader}
      >
        <div style={{ height: "600px" }}>
          <Col span={4}>
            <DropTarget
              targetKey="req"
              onHit={e => {
                this.moveTo(e.dragData, "unClassified");
              }}
            >
              <h3>Вимоги:</h3>
              <div
                className="dropPlace"
                style={{ height: "600px", overflowY: "auto" }}
              >
                {this.state.unClassified.map(x => (
                  <Requirement requirement={x} />
                ))}
              </div>
            </DropTarget>
          </Col>
          <Col offset={2} span={18}>
            <Row style={{ height: "290px" }}>
              <Col span={12} style={{ height: "inherit" }}>
                <DropTarget
                  targetKey="req"
                  onHit={e => {
                    this.moveTo(e.dragData, "business");
                  }}
                >
                  <div
                    className="dropPlace"
                    style={{ backgroundColor: "aquamarine", height: "290px" }}
                  >
                    <h3>Бізнес-вимоги</h3>
                    {this.state.business.map(x => (
                      <Requirement requirement={x} />
                    ))}
                  </div>
                </DropTarget>
              </Col>
              <Col span={12} style={{ height: "inherit" }}>
                <DropTarget
                  targetKey="req"
                  onHit={e => {
                    this.moveTo(e.dragData, "customer");
                  }}
                >
                  <div
                    className="dropPlace"
                    style={{ backgroundColor: "antiquewhite", height: "290px" }}
                  >
                    <h3>Потреби замовника</h3>
                    {this.state.customer.map(x => (
                      <Requirement requirement={x} />
                    ))}
                  </div>
                </DropTarget>
              </Col>
            </Row>

            <Row style={{ height: "290px" }}>
              <Col span={12} style={{ height: "inherit" }}>
                <DropTarget
                  targetKey="req"
                  onHit={e => {
                    this.moveTo(e.dragData, "functional");
                  }}
                >
                  <div
                    className="dropPlace"
                    style={{
                      backgroundColor: "cornflowerblue",
                      height: "290px"
                    }}
                  >
                    <h3>Функціональні вимоги</h3>
                    {this.state.functional.map(x => (
                      <Requirement requirement={x} />
                    ))}
                  </div>
                </DropTarget>
              </Col>
              <Col span={12} style={{ height: "inherit" }}>
                <DropTarget
                  targetKey="req"
                  onHit={e => {
                    this.moveTo(e.dragData, "nonFunctional");
                  }}
                >
                  <div
                    className="dropPlace"
                    style={{
                      backgroundColor: "lightgoldenrodyellow",
                      height: "290px"
                    }}
                  >
                    <h3>Не функціональні вимоги</h3>
                    {this.state.nonFunctional.map(x => (
                      <Requirement requirement={x} />
                    ))}
                  </div>
                </DropTarget>
              </Col>
            </Row>
          </Col>
        </div>
        <Row type="flex" justify="end">
          <Button type="primary" icon="forward" onClick={this.validateAndSave}>
            Порівняти з еталоном
          </Button>
        </Row>
      </PageWrapper>
    );
  }
}

export const Task = withRouter(TaskGeneral);
