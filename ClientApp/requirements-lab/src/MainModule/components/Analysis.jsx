import React, { PureComponent } from "react";

import "../styles/main.css";
import { Row, Col, Icon, Divider, Button } from "antd";
import { withRouter } from "react-router-dom";

class AnalysisGeneral extends PureComponent {
  logout = () => {
    localStorage.removeItem("token");
    this.props.history.push("/login");
  };

  render() {
    return (
      <div className="main-wr">
        <h1>Оберіть категорію класифікації вимог</h1>
        <Row className="upper-row">
          <Col span={6} offset={5}>
            <div
              className="card"
              onClick={() => this.props.history.push("/ierarchical")}
            >
              <i className="fas fa-sitemap" />
              <Divider />
              <h2>
                Класифікація вимог за <br /> ієрархічними рівнями
              </h2>
            </div>
          </Col>
          <Col span={6} offset={2}>
            <div className="card">
              <i className="fas fa-project-diagram" />
              <Divider />
              <h2>
                Класифікація вимог за <br /> характером поведінки
              </h2>
            </div>
          </Col>
        </Row>
        <Row type="flex" justify="center" className="bottom-row">
          <Button type="default" size="large" onClick={this.logout}>
            Вийти
          </Button>
        </Row>
      </div>
    );
  }
}

export const Analysis = withRouter(AnalysisGeneral);
