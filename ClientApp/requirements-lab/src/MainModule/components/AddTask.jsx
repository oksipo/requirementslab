import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { PageWrapper } from "../../common";
import { Row, Button, Col, message, Modal, Input, Form } from "antd";
import { DragDropContainer, DropTarget } from "react-drag-drop-container";
import { Requirement } from "./Requirement";
import { BASE_URL } from "../../BaseConstants";
import "../styles/main.css";

const axios = require("axios");

class AddTaskGeneral extends PureComponent {
  state = {
    unClassified: [],
    business: [],
    customer: [],
    functional: [],
    nonFunctional: [],
    loader: false,
    name: "",
    addCategory: "",
    addName: "",
    addDesc: "",
    taskName: "",
    taskDesk: ""
  };
  uuid = 1;

  moveTo = (requirement, category) => {
    let {
      unClassified,
      business,
      customer,
      functional,
      nonFunctional
    } = this.state;
    unClassified = unClassified.filter(x => x.id !== requirement.id);
    business = business.filter(x => x.id !== requirement.id);
    customer = customer.filter(x => x.id !== requirement.id);
    functional = functional.filter(x => x.id !== requirement.id);
    nonFunctional = nonFunctional.filter(x => x.id !== requirement.id);
    if (category === "business") {
      business.push(requirement);
    }
    if (category === "customer") {
      customer.push(requirement);
    }
    if (category === "functional") {
      functional.push(requirement);
    }
    if (category === "nonFunctional") {
      nonFunctional.push(requirement);
    }
    this.setState({
      unClassified,
      business,
      customer,
      functional,
      nonFunctional
    });
  };

  delete = requirement => {
    let {
      unClassified,
      business,
      customer,
      functional,
      nonFunctional
    } = this.state;
    unClassified = unClassified.filter(x => x.id !== requirement.id);
    business = business.filter(x => x.id !== requirement.id);
    customer = customer.filter(x => x.id !== requirement.id);
    functional = functional.filter(x => x.id !== requirement.id);
    nonFunctional = nonFunctional.filter(x => x.id !== requirement.id);

    this.setState({
      unClassified,
      business,
      customer,
      functional,
      nonFunctional
    });
  };

  add = category => {
    let {
      unClassified,
      business,
      customer,
      functional,
      nonFunctional
    } = this.state;
    const requirement = {
      id: this.uuid++,
      name: this.state.addName,
      description: this.state.addDesc
    };
    if (!this.state.addDesc || !this.state.addName) {
      message.error("Заповніть форму!");
      return;
    }

    if (category === "business") {
      business.push(requirement);
    }
    if (category === "customer") {
      customer.push(requirement);
    }
    if (category === "functional") {
      functional.push(requirement);
    }
    if (category === "nonFunctional") {
      nonFunctional.push(requirement);
    }
    this.setState({
      unClassified,
      business,
      customer,
      functional,
      nonFunctional,
      addName: "",
      addDesc: "",
      addCategory: ""
    });
  };

  validateAndSave = () => {
    this.setState({ loader: true }, () => {
      if (this.state.unClassified.length > 0) {
        message.error("Не всі вимоги класифіковано");
        this.setState({ loader: false });
      } else {
        if (this.state.taskName == "" || this.state.taskDesk === "") {
          message.error("Не всі поля заповнено");
          this.setState({ loader: false });
        } else
          axios
            .post(`${BASE_URL}task`, {
              name: this.state.taskName,
              description: this.state.taskDesk,
              businessReqs: this.state.business,
              customerReqs: this.state.customer,
              functionalReqs: this.state.functional,
              nonFunctionalReqs: this.state.nonFunctional
            })
            .then(({ data }) => {
              console.log(data);
              this.props.history.push(`/ierarchical/`);
            });
      }
    });
  };

  render() {
    return (
      <PageWrapper
        span={16}
        offset={4}
        header={this.state.name}
        isLoading={this.state.loader}
      >
        <div style={{ height: "600px" }}>
          <Col span={4}>
            <DropTarget
              targetKey="req"
              onHit={e => {
                this.moveTo(e.dragData, "unClassified");
              }}
            >
              <h3>Видалити вимогу:</h3>
              <div
                className="dropPlace"
                style={{ height: "300px", overflowY: "auto" }}
              >
                {this.state.unClassified.map(x => (
                  <Requirement
                    requirement={x}
                    onDelete={() => this.delete(x)}
                  />
                ))}
              </div>
            </DropTarget>
            <Form>
              <Form.Item label="Назва завдання">
                <Input
                  value={this.state.taskName}
                  onChange={e => this.setState({ taskName: e.target.value })}
                />
              </Form.Item>
              <Form.Item label="Опис завдання">
                <Input
                  value={this.state.taskDesk}
                  onChange={e => this.setState({ taskDesk: e.target.value })}
                />
              </Form.Item>
            </Form>
          </Col>
          <Col offset={2} span={18}>
            <Row style={{ height: "290px" }}>
              <Col span={12} style={{ height: "inherit" }}>
                <DropTarget
                  targetKey="req"
                  onHit={e => {
                    this.moveTo(e.dragData, "business");
                  }}
                >
                  <div
                    className="dropPlace"
                    style={{ backgroundColor: "aquamarine", height: "290px" }}
                  >
                    <h3>Бізнес-вимоги</h3>
                    {this.state.business.map(x => (
                      <Requirement
                        requirement={x}
                        onDelete={() => this.delete(x)}
                      />
                    ))}
                    <Button
                      type="default"
                      onClick={() => this.setState({ addCategory: "business" })}
                    >
                      +
                    </Button>
                  </div>
                </DropTarget>
              </Col>
              <Col span={12} style={{ height: "inherit" }}>
                <DropTarget
                  targetKey="req"
                  onHit={e => {
                    this.moveTo(e.dragData, "customer");
                  }}
                >
                  <div
                    className="dropPlace"
                    style={{ backgroundColor: "antiquewhite", height: "290px" }}
                  >
                    <h3>Потреби замовника</h3>
                    {this.state.customer.map(x => (
                      <Requirement
                        requirement={x}
                        onDelete={() => this.delete(x)}
                      />
                    ))}
                    <Button
                      type="default"
                      onClick={() => this.setState({ addCategory: "customer" })}
                    >
                      +
                    </Button>
                  </div>
                </DropTarget>
              </Col>
            </Row>

            <Row style={{ height: "290px" }}>
              <Col span={12} style={{ height: "inherit" }}>
                <DropTarget
                  targetKey="req"
                  onHit={e => {
                    this.moveTo(e.dragData, "functional");
                  }}
                >
                  <div
                    className="dropPlace"
                    style={{
                      backgroundColor: "cornflowerblue",
                      height: "290px"
                    }}
                  >
                    <h3>Функціональні вимоги</h3>
                    {this.state.functional.map(x => (
                      <Requirement
                        requirement={x}
                        onDelete={() => this.delete(x)}
                      />
                    ))}
                    <Button
                      type="default"
                      onClick={() =>
                        this.setState({ addCategory: "functional" })
                      }
                    >
                      +
                    </Button>
                  </div>
                </DropTarget>
              </Col>
              <Col span={12} style={{ height: "inherit" }}>
                <DropTarget
                  targetKey="req"
                  onHit={e => {
                    this.moveTo(e.dragData, "nonFunctional");
                  }}
                >
                  <div
                    className="dropPlace"
                    style={{
                      backgroundColor: "lightgoldenrodyellow",
                      height: "290px"
                    }}
                  >
                    <h3>Не функціональні вимоги</h3>
                    {this.state.nonFunctional.map(x => (
                      <Requirement
                        requirement={x}
                        onDelete={() => this.delete(x)}
                      />
                    ))}
                    <Button
                      type="default"
                      onClick={() =>
                        this.setState({ addCategory: "nonFunctional" })
                      }
                    >
                      +
                    </Button>
                  </div>
                </DropTarget>
              </Col>
            </Row>
          </Col>
        </div>
        <Row type="flex" justify="end">
          <Button type="primary" icon="forward" onClick={this.validateAndSave}>
            Додати завдання
          </Button>
        </Row>

        <Modal
          visible={this.state.addCategory}
          onOk={() => this.add(this.state.addCategory)}
          maskClosable={false}
          onCancel={() =>
            this.setState({
              addName: "",
              addDesc: "",
              addCategory: ""
            })
          }
          okText="Додати"
          cancelText="Скасувати"
        >
          <Form>
            <Form.Item label="Назва">
              <Input
                value={this.state.addName}
                onChange={e => this.setState({ addName: e.target.value })}
              />
            </Form.Item>
            <Form.Item label="Опис">
              <Input
                value={this.state.addDesc}
                onChange={e => this.setState({ addDesc: e.target.value })}
              />
            </Form.Item>
          </Form>
        </Modal>
      </PageWrapper>
    );
  }
}

export const AddTask = withRouter(AddTaskGeneral);
