import React, { PureComponent } from "react";

import "../styles/main.css";
import { Row, Col, Icon, Divider, Button } from "antd";
import { withRouter } from "react-router-dom";

class MainGeneral extends PureComponent {
  logout = () => {
    localStorage.removeItem("token");
    this.props.history.push("/login");
  };

  

  render() {
    return (
      <div className="main-wr">
        <h1>Оберіть тип завдання</h1>
        <Row className="upper-row">
          <Col span={6} offset={5}>
            <div
              className="card"
              onClick={() => this.props.history.push("/analysis")}
            >
              <Icon type="ordered-list" />
              <Divider />
              <h2>
                Завдання з <br /> аналізу вимог
              </h2>
            </div>
          </Col>
          <Col span={6} offset={2}>
            <div className="card">
              <i className="fas fa-project-diagram" />
              <Divider />
              <h2>
                Завдання з <br /> проектування системи
              </h2>
            </div>
          </Col>
        </Row>
        <Row>
          <Col span={6} offset={5}>
            <div className="card">
              <i className="fas fa-code" />
              <Divider />
              <h2>
                Завдання з <br /> кодування
              </h2>
            </div>
          </Col>
          <Col span={6} offset={2}>
            <div className="card">
              <i className="fas fa-check-square" />
              <Divider />
              <h2>
                Завдання з <br /> тестування
              </h2>
            </div>
          </Col>
        </Row>
        <Row type="flex" justify="center" className="bottom-row">
          <Button type="default" size="large" onClick={this.logout}>
            Вийти
          </Button>
        </Row>
      </div>
    );
  }
}

export const Main = withRouter(MainGeneral);
