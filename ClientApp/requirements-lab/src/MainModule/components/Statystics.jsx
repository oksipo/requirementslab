import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { PageWrapper } from "../../common";
import { Row, Button } from "antd";
import { BASE_URL } from "../../BaseConstants";

import FusionCharts from "fusioncharts/core";
import StackedColumn2D from "fusioncharts/viz/stackedcolumn2d";
import FusionTheme from "fusioncharts/themes/es/fusioncharts.theme.fusion";
import ReactFC from "react-fusioncharts";
import moment from "moment-with-locales-es6";

ReactFC.fcRoot(FusionCharts, StackedColumn2D, FusionTheme);

const axios = require("axios");

class StatysticsGeneral extends PureComponent {
  state = {
    loader: false
  };

  componentDidMount() {
    console.log(moment.locales());
    this.setState({ loader: true }, () => {
      axios.get(`${BASE_URL}result/statystics`).then(({ data }) => {
        this.setState({ data, loader: false });
      });
    });
  }
  render() {
    const data = this.state.data
      ? {
          chart: {
            caption: "Статистика виконання завдань",
            theme: "fusion",
            plotToolText: "$label: $displayValue"
          },
          categories: [
            {
              category: this.state.data.map(x => {
                let m = moment(x.dateCreated);
                m.locale("uk");
                return {
                  label: m.format("LL")
                };
              })
            }
          ],
          dataset: [
            {
              seriesName: "Правильні відповіді",
              color: "#008000",
              data: this.state.data.map(x => {
                return { value: x.correct, displayValue: x.taskName };
              })
            },
            {
              seriesName: "Помилкові відповіді",
              color: "#FF0000",
              data: this.state.data.map(x => {
                return { value: x.wrong, displayValue: x.taskName };
              })
            }
          ]
        }
      : "";
    return (
      <PageWrapper span={16} offset={4} isLoading={this.state.loader}>
        <div style={{ height: "600px" }}>
          <ReactFC
            type="stackedcolumn2d"
            width="100%"
            height="600"
            dataFormat="JSON"
            dataSource={data}
          />
        </div>
        <Row type="flex" justify="end">
          <Button
            type="default"
            icon="home"
            onClick={() => this.props.history.push("/main")}
          >
            На головну
          </Button>
        </Row>
      </PageWrapper>
    );
  }
}

export const Statystics = withRouter(StatysticsGeneral);
