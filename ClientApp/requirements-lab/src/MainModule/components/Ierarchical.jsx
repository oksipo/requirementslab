import React, { PureComponent } from "react";

import "../styles/main.css";
import { Row, Col, Divider, Button } from "antd";
import { withRouter } from "react-router-dom";

import { BASE_URL } from "../../BaseConstants";

const axios = require("axios");

class IerarchicalGeneral extends PureComponent {
  state = {
    isAdmin: false
  };

  componentDidMount() {
    axios.get(`${BASE_URL}auth/role`).then(({ data }) => {
      this.setState({ isAdmin: data.toLowerCase() === "admin" });
    });
  }

  logout = () => {
    localStorage.removeItem("token");
    this.props.history.push("/login");
  };

  render() {
    return (
      <div className="main-wr">
        {this.state.isAdmin ? (
          <Row className="upper-row">
            <Col span={6} offset={5}>
              <div
                className="card"
                onClick={() => this.props.history.push("/addtask/")}
              >
                <i className="fas fa-plus" />
                <Divider />
                <h2>Додати завдання</h2>
              </div>
            </Col>
            <Col span={6} offset={2}>
              <div
                className="card"
                onClick={() => this.props.history.push("/adminTasks/")}
              >
                <i className="fas fa-pencil-alt" />
                <Divider />
                <h2>Редагувати завдання</h2>
              </div>
            </Col>
          </Row>
        ) : (
          <Row className="upper-row">
            <Col span={6} offset={5}>
              <div
                className="card"
                onClick={() => this.props.history.push("/tasks")}
              >
                <i className="fas fa-pencil-alt" />
                <Divider />
                <h2>Виконати завдання</h2>
              </div>
            </Col>
            <Col span={6} offset={2}>
              <div
                className="card"
                onClick={() => {
                  this.props.history.push("/statystics");
                }}
              >
                <i className="fas fa-poll" />
                <Divider />
                <h2>Переглянути статистику</h2>
              </div>
            </Col>
          </Row>
        )}
        <Row type="flex" justify="center" className="bottom-row">
          <Button type="default" size="large" onClick={this.logout}>
            Вийти
          </Button>
        </Row>
      </div>
    );
  }
}

export const Ierarchical = withRouter(IerarchicalGeneral);
