import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { PageWrapper } from "../../common";
import { Col, Row, Button, Divider } from "antd";
import { BASE_URL } from "../../BaseConstants";

const axios = require("axios");

class ComparisonGeneral extends PureComponent {
  state = {
    loader: false
  };

  componentDidMount() {
    this.setState({ loader: true }, () => {
      axios
        .get(`${BASE_URL}result/${this.props.match.params.id}`)
        .then(({ data }) => {
          this.setState({ data, loader: false });
        });
    });
  }

  render() {
    return (
      <PageWrapper
        span={16}
        offset={4}
        header="Порівняння з еталоном"
        isLoading={this.state.loader}
      >
        <Row style={{ height: "600px", overflowY: "auto" }}>
          <Col
            span={12}
            style={{
              borderRight: "1px solid",
              borderRightColor: "beige",
              height: "100%"
            }}
          >
            <h3>Мій результат</h3>
            <Divider>Бізнес вимоги</Divider>
            <div>
              <i
                style={{
                  textAlign: "start",
                  marginBottom: "5px",
                  float: "left"
                }}
              >
                Наступні вимоги встановлюють призначення програмного
                забезпечення у структурі діяльності організації:
              </i>
              {this.state && this.state.data && this.state.data.my.Bussiness && (
                <ul>
                  {this.state.data.my.Bussiness.map(x => (
                    <li style={{ textAlign: "start" }}>
                      <b style={{ color: x.isCorrect ? "green" : "red" }}>
                        {x.name}
                      </b>
                      <br />
                    </li>
                  ))}
                </ul>
              )}
            </div>
            <Divider>Потреби замовника</Divider>
            <i
              style={{ textAlign: "start", marginBottom: "5px", float: "left" }}
            >
              Наступні вимоги встановлюють набір завдань і можливостей, які має
              реалізувати готовий програмний продукт, а також сценарії їхнього
              вирішення в програмно-апаратній системі:
            </i>
            {this.state && this.state.data && this.state.data.my.Customer && (
              <ul>
                {this.state.data.my.Customer.map(x => (
                  <li style={{ textAlign: "start" }}>
                    <b style={{ color: x.isCorrect ? "green" : "red" }}>
                      {x.name}
                    </b>
                    <br />
                  </li>
                ))}
              </ul>
            )}

            <Divider>Функціональні вимоги</Divider>
            <i
              style={{ textAlign: "start", marginBottom: "5px", float: "left" }}
            >
              Наступні вимоги встановлюють, що має робити готовий програмний
              продукт у програмно-апаратній системі:
            </i>
            {this.state && this.state.data && this.state.data.my.Functional && (
              <ul>
                {this.state.data.my.Functional.map(x => (
                  <li style={{ textAlign: "start" }}>
                    <b style={{ color: x.isCorrect ? "green" : "red" }}>
                      {x.name}
                    </b>
                    <br />
                  </li>
                ))}
              </ul>
            )}
            <Divider>Не функціональні вимоги</Divider>
            <i
              style={{ textAlign: "start", marginBottom: "5px", float: "left" }}
            >
              Наступні вимоги встановлюють, яким готовий програмний продукт має
              бути:
            </i>
            {this.state && this.state.data && this.state.data.my.NonFunctional && (
              <ul>
                {this.state.data.my.NonFunctional.map(x => (
                  <li style={{ textAlign: "start" }}>
                    <b style={{ color: x.isCorrect ? "green" : "red" }}>
                      {x.name}
                    </b>
                    <br />
                  </li>
                ))}
              </ul>
            )}
          </Col>

          <Col span={12} style={{ height: "100%" }}>
            <h3>Еталон</h3>
            <Divider>Бізнес вимоги</Divider>
            <i
              style={{ textAlign: "start", marginBottom: "5px", float: "left" }}
            >
              Наступні вимоги встановлюють призначення програмного забезпечення
              у структурі діяльності організації:
            </i>
            {this.state && this.state.data && this.state.data.etalon.Bussiness && (
              <ul>
                {this.state.data.etalon.Bussiness.map(x => (
                  <li style={{ textAlign: "start" }}>
                    <b style={{ color: x.isCorrect ? "green" : "red" }}>
                      {x.name}
                    </b>
                    <br />
                  </li>
                ))}
              </ul>
            )}
            <Divider>Потреби замовника</Divider>
            <i
              style={{ textAlign: "start", marginBottom: "5px", float: "left" }}
            >
              Наступні вимоги встановлюють набір завдань і можливостей, які має
              реалізувати готовий програмний продукт, а також сценарії їхнього
              вирішення в програмно-апаратній системі:
            </i>

            {this.state && this.state.data && this.state.data.etalon.Customer && (
              <ul>
                {this.state.data.etalon.Customer.map(x => (
                  <li style={{ textAlign: "start" }}>
                    <b style={{ color: x.isCorrect ? "green" : "red" }}>
                      {x.name}
                    </b>
                    <br />
                  </li>
                ))}
              </ul>
            )}
            <Divider>Функціональні вимоги</Divider>
            <i
              style={{ textAlign: "start", marginBottom: "5px", float: "left" }}
            >
              Наступні вимоги встановлюють, що має робити готовий програмний
              продукт у програмно-апаратній системі:
            </i>
            {this.state &&
              this.state.data &&
              this.state.data.etalon.Functional && (
                <ul>
                  {this.state.data.etalon.Functional.map(x => (
                    <li style={{ textAlign: "start" }}>
                      <b style={{ color: x.isCorrect ? "green" : "red" }}>
                        {x.name}
                      </b>
                      <br />
                    </li>
                  ))}
                </ul>
              )}
            <Divider>Не функціональні вимоги</Divider>
            <i
              style={{ textAlign: "start", marginBottom: "5px", float: "left" }}
            >
              Наступні вимоги встановлюють, яким готовий програмний продукт має
              бути:
            </i>

            {this.state &&
              this.state.data &&
              this.state.data.etalon.NonFunctional && (
                <ul>
                  {this.state.data.etalon.NonFunctional.map(x => (
                    <li style={{ textAlign: "start" }}>
                      <b style={{ color: x.isCorrect ? "green" : "red" }}>
                        {x.name}
                      </b>
                      <br />
                    </li>
                  ))}
                </ul>
              )}
          </Col>
        </Row>
        <Row type="flex" justify="end">
          {this.state.data && (
            <p>
              Правильних відповідей: {this.state.data.correct} /{" "}
              {this.state.data.total}{" "}
            </p>
          )}
          <Button
            type="default"
            icon="reload"
            style={{ marginLeft: "5px" }}
            onClick={() =>
              this.props.history.push(`/task/${this.state.data.taskId}`)
            }
          >
            Повторити
          </Button>
          <Button
            type="primary"
            icon="forward"
            style={{ marginLeft: "5px" }}
            onClick={() => this.props.history.push(`/statystics/`)}
          >
            Переглянути статистику
          </Button>
        </Row>
      </PageWrapper>
    );
  }
}

export const Comparison = withRouter(ComparisonGeneral);
