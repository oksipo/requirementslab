import React, { PureComponent } from "react";
import { withRouter } from "react-router-dom";
import { PageWrapper } from "../../common";
import { Row, Button, Col, message, Collapse, Divider } from "antd";
import { DragDropContainer, DropTarget } from "react-drag-drop-container";
import { Requirement } from "./Requirement";
import { BASE_URL } from "../../BaseConstants";
import "../styles/main.css";

const axios = require("axios");

class AdminTasksGeneral extends PureComponent {
  state = {
    tasks: [],
    loader: false
  };

  componentDidMount() {
    this.setState({ loader: true }, () => {
      axios.get(`${BASE_URL}task/`).then(({ data }) => {
        this.setState({
          tasks: data,
          loader: false
        });
      });
    });
  }

  delete = id => {
    this.setState({ loader: true }, () => {
      axios.delete(`${BASE_URL}task/${id}`).then(() => {
        this.setState({
          tasks: this.state.tasks.filter(x => x.id !== id),
          loader: false
        });
      });
    });
  };

  render() {
    return (
      <PageWrapper
        span={16}
        offset={4}
        header="Оберіть завдання"
        isLoading={this.state.loader}
      >
        <div style={{ height: "600px", overflowY: "auto" }}>
          <Collapse>
            {this.state.tasks.map(x => (
              <Collapse.Panel header={x.name}>
                <p style={{ textAlign: "left" }}>
                  <b>Опис: </b>
                  {x.description}
                </p>
                <Divider style={{ margin: "2px" }} />
                <Button
                  type="primary"
                  style={{
                    float: "right",
                    marginBottom: "5px",
                    marginRight: "5px",
                    marginLeft: "5px"
                  }}
                  onClick={() => this.delete(x.id)}
                >
                  <i className="fas fa-trash" style={{ marginRight: "4px" }} />
                  {"    Видалити"}
                </Button>

                <Button
                  type="primary"
                  style={{ float: "right", marginBottom: "5px" }}
                  onClick={() => this.props.history.push(`/editTask/${x.id}`)}
                >
                  <i
                    className="fas fa-pencil-alt"
                    style={{ marginRight: "4px" }}
                  />
                  {"    Редагувати"}
                </Button>
              </Collapse.Panel>
            ))}
          </Collapse>
        </div>
      </PageWrapper>
    );
  }
}

export const AdminTasks = withRouter(AdminTasksGeneral);
