﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Models.Entities;
using WebApplication1.Models.Auth;
using Task = System.Threading.Tasks.Task;

namespace WebApplication1.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private UserManager<User> userManager;
        private SignInManager<User> signInManager;

        public AuthController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            var identity = await this.userManager.CreateAsync(new User()
            {
                Email = model.Email,
                UserName = model.Email,
                FullName = model.FullName,
                Results = new List<Result>()
            }, model.Password);
            if (identity.Succeeded)
            {
                var user = await userManager.FindByEmailAsync(model.Email);
                await userManager.AddToRoleAsync(user, model.IsAdmin ? "admin" : "student");

                await signInManager.SignInAsync(user, false);

                return Ok(new JwtSecurityTokenHandler().WriteToken(GetToken(user)));
            }
            return BadRequest();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var user = await userManager.FindByEmailAsync(model.Email);
            var result = await userManager.CheckPasswordAsync(user, model.Password);
            if (result) { 
                await signInManager.SignInAsync(user, false);

                return Ok(new JwtSecurityTokenHandler().WriteToken(GetToken(user)));
            }
            return BadRequest();
        }

        [Authorize]
        [HttpGet("IsLoggedIn")]
        public async Task<IActionResult> IsLoggedIn()
        {
            return Ok(User.Identity != null);
        }

        [Authorize]
        [HttpGet("role")]
        public IActionResult GetRole()
        {
            return Ok(User.Claims.FirstOrDefault(x => x.Type == "role").Value);
        }

        private JwtSecurityToken GetToken(User user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("NetCoreToldMeThatKeyShouldBeMoreThan128Bits"));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(Convert.ToDouble(30000));
            var claims = new List<Claim>()
            {
                new Claim("email",user.Email),
                new Claim("name",user.FullName),
                new Claim("role", userManager.GetRolesAsync(user).Result.FirstOrDefault())
            };
            var token = new JwtSecurityToken(
                claims: claims,
                expires: expires,
                signingCredentials: creds
            );

            return token;
        }
    }


}