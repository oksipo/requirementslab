﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Entities;
using Models.Enums;
using WebApplication1.Data;
using WebApplication1.Factories;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {

        private readonly ApplicationDbContext _context;
        private readonly TaskModelFactory _taskFactory = new TaskModelFactory();

        public TaskController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult GetTask([FromRoute] long id)
        {

            var taskEntity = _context.Tasks.Include(x => x.Requirements).FirstOrDefault(x => x.Id == id);
            var task = _taskFactory.PrepareTaskViewModel(taskEntity);

            return Ok(task);
        }

        [HttpGet]
        public IActionResult GetAllTasks()
        {
            var tasks = new List<TaskViewModel>();

            var taskEntities = _context.Tasks.Include(x => x.Requirements).ToList();
            foreach (var task in taskEntities)
            {
                tasks.Add(_taskFactory.PrepareTaskViewModel(task));
            }

            return Ok(tasks);
        }

        [HttpGet("{taskId}")]
        public IActionResult GetRequirements([FromRoute] long taskId)
        {
            var reqs = _context.Tasks.Find(taskId).Requirements.ToList();

            var requirements = new List<RequirementViewModel>();
            foreach (var req in reqs)
            {
                requirements.Add(_taskFactory.PrepareRequirementViewModel(req));
            }

            return Ok(requirements);
        }

        [HttpGet("adminTask/{id}")]
        public IActionResult GetAdminTask([FromRoute] long id)
        {
            var t = _context.Tasks.Include(x => x.Requirements).ThenInclude(x => x.ExpectedResult).FirstOrDefault(x => x.Id == id);
            return Ok(new TaskEditModel()
            {
                Id = id,
                Name = t.Name,
                Description = t.Description,
                BusinessReqs = t.Requirements.Where(x => x.ExpectedResult.Category == Category.Bussiness).Select(x =>
                    new RequirementCreateModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Name
                    }).ToList(),
                NonFunctionalReqs = t.Requirements.Where(x => x.ExpectedResult.Category == Category.NonFunctional)
                    .Select(x => new RequirementCreateModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Name
                    }).ToList(),
                FunctionalReqs = t.Requirements.Where(x => x.ExpectedResult.Category == Category.Functional).Select(x =>
                    new RequirementCreateModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Name
                    }).ToList(),
                CustomerReqs = t.Requirements.Where(x => x.ExpectedResult.Category == Category.Customer).Select(x =>
                    new RequirementCreateModel()
                    {
                        Id = x.Id,
                        Name = x.Name,
                        Description = x.Name
                    }).ToList()
            });
        }

        [HttpPost("result")]
        public IActionResult CreateResult([FromBody] ResultCreateModel model)
        {
            var currentUser = _context.Users.FirstOrDefault(x => x.Email == User.Identity.Name);
            var newRes = new Result()
            {
                User = currentUser,
                TaskId = model.TaskId,
                DateCreated = DateTime.Now,
                RealResults = model.BusinessRequirements.Select(x => new RealResult()
                {
                    RequirementId = x,
                    Category = Category.Bussiness
                }).Concat(
                    model.CustomerRequirements.Select(x => new RealResult()
                    {
                        RequirementId = x,
                        Category = Category.Customer
                    })).Concat(
                    model.FunctionalRequirements.Select(x => new RealResult()
                    {
                        RequirementId = x,
                        Category = Category.Functional
                    })).Concat(
                    model.NonFunctionalRequirements.Select(x => new RealResult()
                    {
                        RequirementId = x,
                        Category = Category.NonFunctional
                    })).ToList()
            };
            _context.Results.Add(newRes);
            _context.SaveChanges();
            return Ok(newRes.Id);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTask([FromRoute] long id)
        {
            var t = _context.Tasks.Include(x=>x.Requirements).ThenInclude(x=>x.ExpectedResult).Include(x=>x.Results).ThenInclude(x=>x.RealResults).FirstOrDefault(x => x.Id == id);
            _context.RealResults.RemoveRange(t.Results.SelectMany(x => x.RealResults));
            _context.Results.RemoveRange(t.Results);
            _context.ExpectedResult.RemoveRange(t.Requirements.Select(x=>x.ExpectedResult));
            _context.Requirements.RemoveRange(t.Requirements);
            _context.Tasks.Remove(t);
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost]
        public IActionResult CreateTask([FromBody] TaskCreateModel model)
        {
            var task = new Task()
            {
                Description = model.Description,
                Name = model.Name,
                Requirements = model.BusinessReqs.Select(x => new Requirement()
                {
                    Name = x.Name,
                    Description = x.Description,
                    ExpectedResult = new ExpectedResult()
                    {
                        Category = Category.Bussiness
                    }
                }).Concat(model.CustomerReqs.Select(x => new Requirement()
                {
                    Name = x.Name,
                    Description = x.Description,
                    ExpectedResult = new ExpectedResult()
                    {
                        Category = Category.Customer
                    }
                })).Concat(model.FunctionalReqs.Select(x => new Requirement()
                {
                    Name = x.Name,
                    Description = x.Description,
                    ExpectedResult = new ExpectedResult()
                    {
                        Category = Category.Functional
                    }
                })).Concat(model.NonFunctionalReqs.Select(x => new Requirement()
                {
                    Name = x.Name,
                    Description = x.Description,
                    ExpectedResult = new ExpectedResult()
                    {
                        Category = Category.NonFunctional
                    }
                })).ToList()
            };
            _context.Tasks.Add(task);
            _context.SaveChanges();
            return Ok(task.Id);
        }

        public IActionResult GetCategories()
        {
            var categories = Enum.GetNames(typeof(Category)).ToDictionary(name => (int)Enum.Parse(typeof(Category), name));

            return Ok(categories);
        }
    }
}