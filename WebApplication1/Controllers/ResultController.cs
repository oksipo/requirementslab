﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Models.Enums;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ResultController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ResultController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute] long id)
        {
            var res = _context.Results.Include(x => x.RealResults).ThenInclude(x => x.Requirement.ExpectedResult)
                .FirstOrDefault(x => x.Id == id);
            var forReturn = new ResultViewModel()
            {
                TaskId = res.TaskId,
                Id = res.Id,
                My = new Dictionary<Category, List<ResultRequirementViewModel>>(),
                Etalon = new Dictionary<Category, List<ResultRequirementViewModel>>(),
                Correct = 0,
                Total = 0
            };

            foreach (var req in res.RealResults)
            {
                forReturn.Total++;
                if (!forReturn.My.ContainsKey(req.Category))
                {
                    forReturn.My.Add(req.Category, new List<ResultRequirementViewModel>());
                }
                if (!forReturn.Etalon.ContainsKey(req.Requirement.ExpectedResult.Category))
                {
                    forReturn.Etalon.Add(req.Requirement.ExpectedResult.Category, new List<ResultRequirementViewModel>());
                }

                bool isCorrect = req.Category == req.Requirement.ExpectedResult.Category;
                forReturn.My[req.Category].Add(new ResultRequirementViewModel()
                {
                    Id = req.Id,
                    Name = req.Requirement.Name,
                    isCorrect = isCorrect
                });
                forReturn.Etalon[req.Requirement.ExpectedResult.Category].Add(new ResultRequirementViewModel()
                {
                    Id = req.Id,
                    Name = req.Requirement.Name,
                    isCorrect = isCorrect
                });
                if (isCorrect)
                {
                    forReturn.Correct++;
                }
            }

            return Ok(forReturn);
        }

        [HttpGet("statystics")]
        public IActionResult GetStatystics()
        {
            var results = _context.Results.Include(x=>x.Task).Include(x=>x.RealResults).ThenInclude(x=>x.Requirement.ExpectedResult).Where(x => x.User.Email == User.Identity.Name);
            var res = new List<StatisticsModel>();
            foreach (var result in results)
            {
                var correct = 0;
                var wrong = 0;
                foreach (var req in result.RealResults)
                {
                    var isCorrect = req.Category == req.Requirement.ExpectedResult.Category;
                    if (isCorrect)
                    {
                        correct++;
                    }
                    else
                    {
                        wrong++;
                    }
                }
                res.Add(new StatisticsModel()
                {
                    TaskName = result.Task.Name,
                    Correct = correct,
                    Wrong = wrong,
                    DateCreated = result.DateCreated
                });
            }

            return Ok(res);
        }
    }
}
