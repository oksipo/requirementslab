﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApplication1.Data.Migrations
{
    public partial class removestupidforeignkey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExpectedResult_Requirements_RequirementId",
                table: "ExpectedResult");

            migrationBuilder.DropIndex(
                name: "IX_Requirements_ExpectedResultId",
                table: "Requirements");

            migrationBuilder.DropIndex(
                name: "IX_ExpectedResult_RequirementId",
                table: "ExpectedResult");

            migrationBuilder.DropColumn(
                name: "RequirementId",
                table: "ExpectedResult");

            migrationBuilder.CreateIndex(
                name: "IX_Requirements_ExpectedResultId",
                table: "Requirements",
                column: "ExpectedResultId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Requirements_ExpectedResultId",
                table: "Requirements");

            migrationBuilder.AddColumn<long>(
                name: "RequirementId",
                table: "ExpectedResult",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Requirements_ExpectedResultId",
                table: "Requirements",
                column: "ExpectedResultId");

            migrationBuilder.CreateIndex(
                name: "IX_ExpectedResult_RequirementId",
                table: "ExpectedResult",
                column: "RequirementId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExpectedResult_Requirements_RequirementId",
                table: "ExpectedResult",
                column: "RequirementId",
                principalTable: "Requirements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
