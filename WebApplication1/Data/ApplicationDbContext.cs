﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Models.Entities;

namespace WebApplication1.Data
{
    public class ApplicationDbContext : IdentityDbContext<User,IdentityRole,string>
    {
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Requirement> Requirements { get; set; }
        public DbSet<ExpectedResult> ExpectedResult { get; set; }
        public DbSet<Result> Results { get; set; }
        public DbSet<RealResult> RealResults { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }
    }
}