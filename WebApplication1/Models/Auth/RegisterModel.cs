﻿namespace WebApplication1.Models.Auth
{
    public class RegisterModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string FullName { get; set; }

        public bool IsAdmin { get; set; }
    }
}
