﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models.Enums;

namespace WebApplication1.Models
{
    public class ResultViewModel
    {
        public long Id { get; set; }

        public long TaskId { get; set; }

        public long Correct { get; set; }

        public long Total { get; set; }

        public Dictionary<Category,List<ResultRequirementViewModel>> My { get; set; }
        public Dictionary<Category,List<ResultRequirementViewModel>> Etalon { get; set; }
    }
}
