﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class ResultCreateModel
    {
        public long TaskId { get; set; }

        public List<long> BusinessRequirements { get; set; }

        public List<long> CustomerRequirements { get; set; }

        public List<long> FunctionalRequirements { get; set; }

        public List<long> NonFunctionalRequirements { get; set; }
    }
}
