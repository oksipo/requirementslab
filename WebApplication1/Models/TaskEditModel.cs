﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class TaskEditModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public List<RequirementCreateModel> BusinessReqs { get; set; } = new List<RequirementCreateModel>();
        public List<RequirementCreateModel> CustomerReqs { get; set; } = new List<RequirementCreateModel>();
        public List<RequirementCreateModel> FunctionalReqs { get; set; } = new List<RequirementCreateModel>();
        public List<RequirementCreateModel> NonFunctionalReqs { get; set; } = new List<RequirementCreateModel>();
    }
}
