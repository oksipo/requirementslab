﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class StatisticsModel
    {
        public string TaskName { get; set; }

        public long Correct { get; set; }

        public long Wrong { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
