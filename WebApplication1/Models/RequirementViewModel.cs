﻿
namespace WebApplication1.Models
{
    public class RequirementViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long TaskId { get; set; }
        public long ExptectedCategoryId { get; set; }
    }
}
