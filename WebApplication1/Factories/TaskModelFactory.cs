﻿using Models.Entities;
using System;
using System.Linq;
using WebApplication1.Models;
using Task = Models.Entities.Task;

namespace WebApplication1.Factories
{
    public class TaskModelFactory
    {
        public TaskViewModel PrepareTaskViewModel(Task task)
        {
            var reqs = task.Requirements.ToList().Select(x => new RequirementViewModel
            {
                Id = x.Id,
                Name = x.Name,
                Description = x.Description,
                TaskId = x.TaskId
            });

            return new TaskViewModel()
            {
                Id = task.Id,
                Description = task.Description,
                Name = task.Name,
                Requirements = reqs.ToList()
            };
        }

        public RequirementViewModel PrepareRequirementViewModel(Requirement requirement)
        {
            return  new RequirementViewModel()
            {
                Id = requirement.Id,
                Name = requirement.Name,
                Description = requirement.Description,
                TaskId = requirement.TaskId,
            };
        }
    }
}
